# README #
##
- `docker pull bitnami/cassandra`
##
- `docker network create app-tier --driver bridge`
- `docker run -p 9042:9042 --name cassandra-server --network app-tier bitnami/cassandra:latest`
- `docker run -it --rm --network app-tier bitnami/cassandra:latest cqlsh --username cassandra --password cassandra cassandra-server`
##
- `CREATE KEYSPACE carx WITH REPLICATION = {'class':'SimpleStrategy', 'replication_factor':1};`
- `CREATE TABLE carx.players (UUID text, country ascii, money int, PRIMARY KEY (UUID));`
- `CREATE TABLE carx.activities (player text, activity int, time timestamp, PRIMARY KEY (player, activity, time));`
- `CREATE TABLE carx.registrations (player text, country ascii, time timestamp, PRIMARY KEY (player));`