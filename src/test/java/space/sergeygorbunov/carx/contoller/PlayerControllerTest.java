package space.sergeygorbunov.carx.contoller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import space.sergeygorbunov.carx.contoller.data.PlayerData;
import space.sergeygorbunov.carx.entity.player.Player;
import space.sergeygorbunov.carx.service.PlayerService;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/17/2020
 * Time: 12:22 AM
 * To change this template use File | Settings | File and Code Templates.
 */
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class PlayerControllerTest {

    @Autowired
    private MockMvc perform;

    @Autowired
    private PlayerController controller;

    @Autowired
    private PlayerService service;

    private Player player;

    @Before
    public void before() {
        player = new Player("7685ccf4-81e6-44b9-a09b-42b7d0541599", "EN", 100);
    }

    @Test
    public void getControllerLayer() throws Exception {

        // add
        service.save(player);

        // check
        Assert.assertNotNull(service.get(player.getUUID()));

        // request
        PlayerData data = controller.get("7685ccf4-81e6-44b9-a09b-42b7d0541599");

        // assert
        Assert.assertNotNull(data);
        Assert.assertEquals(player.getUUID(), data.getUUID());
        Assert.assertEquals(player.getCountry(), data.getCountry());
        Assert.assertEquals(player.getMoney(), data.getMoney());
    }

    @Test
    public void getHttpLayer() throws Exception {

        // add
        service.save(player);

        // check
        perform
                .perform(get(String.format("/player/%s", player.getUUID())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(player.getUUID())));
    }
}