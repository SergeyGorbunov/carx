package space.sergeygorbunov.carx.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;
import space.sergeygorbunov.carx.entity.registration.Registration;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 12:07 PM
 * To change this template use File | Settings | File and Code Templates.
 */
@Repository
public interface RegistrationRepository extends CassandraRepository<Registration, String> {
}
