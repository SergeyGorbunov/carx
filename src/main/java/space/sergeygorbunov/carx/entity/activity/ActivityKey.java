package space.sergeygorbunov.carx.entity.activity;

import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import java.io.Serializable;
import java.util.Date;

import static org.springframework.data.cassandra.core.cql.PrimaryKeyType.PARTITIONED;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 11:38 AM
 * To change this template use File | Settings | File and Code Templates.
 */
@PrimaryKeyClass
public class ActivityKey implements Serializable {

    @PrimaryKeyColumn(type = PARTITIONED)
    private String player;

    @PrimaryKeyColumn
    private int activity;

    @PrimaryKeyColumn
    private Date time;

    public ActivityKey() {
    }

    public ActivityKey(String player, int activity, Date time) {
        this.player = player;
        this.activity = activity;
        this.time = time;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityKey that = (ActivityKey) o;
        if (activity != that.activity) return false;
        if (!player.equals(that.player)) return false;
        return time.equals(that.time);
    }

    @Override
    public int hashCode() {
        int result = player.hashCode();
        result = 31 * result + activity;
        result = 31 * result + time.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ActivityKey{" +
                "player='" + player + '\'' +
                ", activity=" + activity +
                ", time=" + time +
                '}';
    }
}
