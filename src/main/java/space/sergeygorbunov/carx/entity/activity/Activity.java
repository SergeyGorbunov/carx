package space.sergeygorbunov.carx.entity.activity;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 12:14 AM
 * To change this template use File | Settings | File and Code Templates.
 */
@Table("activities")
public class Activity {

    @PrimaryKey
    private ActivityKey key;

    public Activity() {
    }

    public Activity(ActivityKey key) {
        this.key = key;
    }

    public ActivityKey getKey() {
        return key;
    }

    public void setKey(ActivityKey key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "key=" + key +
                '}';
    }
}
