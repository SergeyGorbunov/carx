package space.sergeygorbunov.carx.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import space.sergeygorbunov.carx.contoller.data.PlayerData;
import space.sergeygorbunov.carx.entity.player.Player;
import space.sergeygorbunov.carx.entity.registration.Registration;
import space.sergeygorbunov.carx.service.PlayerService;
import space.sergeygorbunov.carx.service.RegistrationService;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/15/2020
 * Time: 11:35 PM
 * To change this template use File | Settings | File and Code Templates.
 */
@RestController
@RequestMapping("/player")
public class PlayerController {

    @Autowired
    private PlayerService service;

    @Autowired
    private RegistrationService registration;

    @PutMapping
    public ResponseEntity<?> put(@RequestBody PlayerData data) {
        service.save(new Player(data.getUUID(), data.getCountry(), data.getMoney()));
        registration.save(new Registration(data.getUUID(), data.getCountry()));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{UUID}")
    public PlayerData get(@PathVariable("UUID") String UUID) {
        Player player = service.get(UUID);
        return new PlayerData(player.getUUID(), player.getCountry(), player.getMoney());
    }
}
