package space.sergeygorbunov.carx.contoller.exception;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 11/13/2015
 * Time: 2:14 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class ExceptionControllerError {
    private String error;

    public ExceptionControllerError(Exception e) {
        this.error = String.format("%s, %s, %s",
                e.getMessage(),
                e.getCause().getMessage(),
                e.getCause().getCause().getMessage());
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ExceptionError{" +
                "error='" + error + '\'' +
                '}';
    }
}
