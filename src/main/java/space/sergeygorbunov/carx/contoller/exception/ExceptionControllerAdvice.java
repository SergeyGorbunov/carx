package space.sergeygorbunov.carx.contoller.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 11/13/2015
 * Time: 1:12 PM
 * To change this template use File | Settings | File and Code Templates.
 */
@ControllerAdvice
public class ExceptionControllerAdvice {
    private static final Logger LOG = LoggerFactory.getLogger(ExceptionControllerAdvice.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionControllerError> Exception(Exception e) {
        return new ResponseEntity<>(new ExceptionControllerError(e), HttpStatus.BAD_REQUEST);
    }
}
