package space.sergeygorbunov.carx.contoller.data;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 1:13 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class PlayerData {

    private String UUID;
    private String country;
    private int money;

    public PlayerData() {
    }

    public PlayerData(String UUID, String country, int money) {
        this.UUID = UUID;
        this.country = country;
        this.money = money;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "PlayerData{" +
                "UUID='" + UUID + '\'' +
                ", country='" + country + '\'' +
                ", money=" + money +
                '}';
    }
}
