package space.sergeygorbunov.carx.contoller.data;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 1:13 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class ActivityData {

    private String UUID;
    private int activity;

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return "ActivityData{" +
                "UUID='" + UUID + '\'' +
                ", activity=" + activity +
                '}';
    }
}
