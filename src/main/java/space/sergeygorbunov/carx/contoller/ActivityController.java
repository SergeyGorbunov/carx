package space.sergeygorbunov.carx.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.sergeygorbunov.carx.contoller.data.ActivityData;
import space.sergeygorbunov.carx.entity.activity.Activity;
import space.sergeygorbunov.carx.entity.activity.ActivityKey;
import space.sergeygorbunov.carx.service.ActivityService;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/15/2020
 * Time: 11:35 PM
 * To change this template use File | Settings | File and Code Templates.
 */
@RestController
@RequestMapping("/activity")
public class ActivityController {

    @Autowired
    private ActivityService service;

    @PutMapping
    public ResponseEntity<?> put(@RequestBody ActivityData data) {
        service.save(new Activity(new ActivityKey(data.getUUID(), data.getActivity(), new Date())));
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
