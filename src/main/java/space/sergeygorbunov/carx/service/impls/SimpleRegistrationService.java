package space.sergeygorbunov.carx.service.impls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.InsertOptions;
import org.springframework.stereotype.Service;
import space.sergeygorbunov.carx.entity.registration.Registration;
import space.sergeygorbunov.carx.service.RegistrationService;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/19/2020
 * Time: 1:09 AM
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class SimpleRegistrationService implements RegistrationService {

    @Autowired
    private CassandraOperations operations;

    @Override
    public void save(Registration registration) {
        operations.insert(registration, InsertOptions.builder().ifNotExists(true).build());
    }
}
