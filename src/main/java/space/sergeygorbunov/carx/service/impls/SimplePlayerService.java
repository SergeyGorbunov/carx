package space.sergeygorbunov.carx.service.impls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import space.sergeygorbunov.carx.entity.player.Player;
import space.sergeygorbunov.carx.repository.PlayerRepository;
import space.sergeygorbunov.carx.service.PlayerService;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 11:27 AM
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class SimplePlayerService implements PlayerService {

    @Autowired
    private PlayerRepository repository;

    @Override
    public void save(Player player) {
        repository.save(player);
    }

    @Override
    public Player get(String UUID) {
        return repository
                .findById(UUID)
                .orElse(new Player("Not found player", "Not found country", 0));
    }
}
