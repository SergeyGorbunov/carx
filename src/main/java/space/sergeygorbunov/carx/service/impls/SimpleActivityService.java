package space.sergeygorbunov.carx.service.impls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import space.sergeygorbunov.carx.entity.activity.Activity;
import space.sergeygorbunov.carx.repository.ActivityRepository;
import space.sergeygorbunov.carx.service.ActivityService;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 11:27 AM
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class SimpleActivityService implements ActivityService {

    @Autowired
    private ActivityRepository repository;

    @Override
    public void save(Activity activity) {
        repository.save(activity);
    }
}
