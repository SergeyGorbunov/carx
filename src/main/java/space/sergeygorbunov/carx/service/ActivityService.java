package space.sergeygorbunov.carx.service;

import space.sergeygorbunov.carx.entity.activity.Activity;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 11:25 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface ActivityService {
    void save(Activity activity);
}
